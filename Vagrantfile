# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 1.8.5"

require 'yaml'

current_dir    = File.dirname(File.expand_path(__FILE__))
configs        = YAML.load_file("#{current_dir}/VagrantConfig.yml")
vagrant_config = configs
server_user = "bitrix"

Vagrant.configure("2") do |config|

    config.vm.provider :virtualbox do |v|
        v.name = vagrant_config['host']
        v.customize [
            "modifyvm", :id,
            "--name", vagrant_config['host'],
            "--memory", vagrant_config['memory'],
            "--natdnshostresolver1", "on",
            "--cpus", vagrant_config['cpus'],
        ]
    end

    config.vm.box = "ubuntu/trusty64"

    config.vm.network :private_network, ip: vagrant_config['ip']
    config.ssh.forward_agent = true

    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "ansible/vagrant.yml"
        ansible.inventory_path = "ansible/inventory/hosts"
        ansible.limit = 'vagrant'
        ansible.extra_vars = {vagrant: vagrant_config}
    end

    config.vm.synced_folder "./",  "/vagrant", id: "vagrant-root"
    config.vm.synced_folder "./share",  "/var/www/#{server_user}/data", id: "vagrant-root" # todo: connect to data folder, not to public
    # config.vm.synced_folder "./", "/var/www/#{server_user}",  type: vagrant_config['synced_type']
end
